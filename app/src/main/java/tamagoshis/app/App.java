/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package tamagoshis.app;

import tamagoshis.list.LinkedList;

import static tamagoshis.utilities.StringUtils.join;
import static tamagoshis.utilities.StringUtils.split;
import static tamagoshis.app.MessageUtils.getMessage;

import org.apache.commons.text.WordUtils;

public class App {
    public static void main(String[] args) {
        LinkedList tokens;
        tokens = split(getMessage());
        String result = join(tokens);
        System.out.println(WordUtils.capitalize(result));
    }
}
